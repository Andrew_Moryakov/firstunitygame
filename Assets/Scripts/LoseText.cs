﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class LoseText : MonoBehaviour
{
	private Text text;

	void Awake()
	{
		text = GetComponent<Text>();
	}

	void Start()
	{
		text.text = "You lose";
	}
}
