﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
namespace Assets.Scripts
{
	public class ScoreManager : MonoBehaviour
	{

		private Text textScore;

		public static float Score = 10f;
		// Use this for initialization
		void Start()
		{
			Score = 10f;
		}

		void Awake()
		{
			textScore = GetComponent<Text>();
		}

		// Update is called once per frame
		void Update()
		{
			textScore.text = string.Format("Score: {0}", ((int)Score).ToString());
		}
	}
}