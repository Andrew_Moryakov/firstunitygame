﻿using UnityEngine;

namespace Assets.Scripts
{
	public static class Helpers
	{
		public static T GetInstanteGameObject<T>(RaycastHit hit)
		{
			return hit.collider.GetComponent<T>();
		}
	}
}