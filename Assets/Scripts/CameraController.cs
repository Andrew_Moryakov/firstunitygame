﻿using System;
using System.Collections;
using System.Linq;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using Random = UnityEngine.Random;

namespace Assets.Scripts
{
	public class CameraController : MonoBehaviour
	{
		private Renderer rend;
		public float respawnTime = 1.0f;
		private float plusScore = 10;
		private float increaseScore = 20;
		private float winScore = 100f;

		public Transform _particalSystem;

		private IEnumerator RespawnWait()
		{
			rend.enabled = false;

			yield return  new WaitForSeconds(respawnTime);
			
			rend.enabled = true;
		}

		void Awake()
		{
			rend = GetComponent<Renderer>();
		}

		void LaterUpdate()
		{
		 Destroy(_particalSystem);
		}

		void Update()
		{
			StopChangeScore();
			DecreaseScore();
			Win();

			if (Input.GetMouseButtonDown(0))
			{
				RaycastHit hit;
				Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

				if (Physics.Raycast(ray, out hit, 100))
				{
					Instantiate(_particalSystem,  new Vector3(hit.transform.position.x, hit.transform.position.y, -7), hit.transform.rotation);
					ScoreManager.Score += plusScore;

					rend = hit.collider.GetComponent<Renderer>();
					StartCoroutine(RespawnWait());

					hit.transform.position = GameSemantic.NewRandomPos();

					Color colorValue = GetRandomColorOfTheHit(hit);
					hit.transform.gameObject.GetComponent<Renderer>().material.SetColor("_EmissionColor", colorValue);
				}
				else
				{
					ScoreManager.Score -= increaseScore;
					
				}

			}
		}

		void Win()
		{
			if (ScoreManager.Score >= winScore)
			{
				SceneManager.LoadScene("YouWin");
			}
		}

		void DecreaseScore()
		{
			ScoreManager.Score -= 0.1f;
		}

		private void Lose()
		{
			SceneManager.LoadScene("Lose");
		}
		void StopChangeScore()
		{
			if (ScoreManager.Score<=0)
			{
				ScoreManager.Score = 0;
				Lose();
				print("lose");
			}
		}

		public Color GetRandomColorOfTheHit(RaycastHit hit)
		{
			switch (hit.transform.tag)
			{
				case "Player":
				{
					Player gameObj = Helpers.GetInstanteGameObject<Player>(hit);
					Color[] colors = gameObj.ColorsPlayer;
					return colors[Random.Range(0, colors.Length)];
				}
				case "Enemy":
				{
						Enemy gameObj = Helpers.GetInstanteGameObject<Enemy>(hit);
						Color[] colors = gameObj.ColorsEnemy;
						return colors[Random.Range(0, colors.Length)];
				}
				default:
				{
					throw new Exception();
				}
			}
		}
	}
}