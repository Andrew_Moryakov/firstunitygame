﻿using UnityEngine;

namespace Assets.Scripts
{
	public class PartiacalSystem : MonoBehaviour
	{
		private ParticleSystem partiacal;
		void Awake()
		{
			partiacal = GetComponent<ParticleSystem>();
		}


		void LateUpdate()
		{
			if (!partiacal.IsAlive())
			{
				Destroy(gameObject);
				
			}
		}
	}
}
