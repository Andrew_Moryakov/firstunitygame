﻿using UnityEngine;
using UnityEngine.SceneManagement;

namespace Assets.Scripts
{
	public class MainMenuScript : MonoBehaviour
	{

		void OnGUI()
		{
			if (GUI.Button(new Rect(10, 10, 50, 50), "Start"))
			{
				SceneManager.LoadScene("Game");
			}

			if (GUI.Button(new Rect(10, 80, 50, 50), "Exit"))
			{
			Application.Quit();
			}
		}
	}
}
