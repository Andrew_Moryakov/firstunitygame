﻿using UnityEngine;
using System.Collections;

namespace Assets.Scripts
{
	public static class GameSemantic
	{
		public static Vector3 NewRandomPos()
		{
			return new Vector3(Random.Range(-3.65f, 3.02f), Random.Range(3.18f, -2.18f));
		}
		 
	}
}
